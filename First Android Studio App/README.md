> **NOTE:** This README.md file goes over and displays Applications and code.

# Android Studio First Basic App

## Thomas Palmieri

### First App HelloWorld Android Studio:

3. complete hello world java

#### README.md file should include the following items:

* First Simple Application pictures

#### App Screenshots:

*Screenshot of Android Studio - My First App*:

| Hello World First picture    | Hello World Second picture   |
| ---------------------------- | ---------------------------- |
| ![](HelloWorld1stScreen.PNG) | ![](HelloWorld2ndScreen.PNG) |

Source Code

| First Contact Page             | Second Contact Page             |
| ------------------------------ | ------------------------------- |
| ![](HelloWorldAndroidCode.PNG) | ![](HelloWorldAndroidCode2.PNG) |



#### Links to .txt Source:

[Hello World Main](HelloWorldMain.txt)

[Hello World Button](HelloWorldButton.txt)

