# Currency Converter App

## Thomas Palmieri

### Currency Converter App Requirements:

1. Field to enter U.S. dollar amount : 1-100,000
2. Must Have toast notification
3. Must have Correct sign for currency
4. Must add background color
6. have SPLASH screen
7. Screenshots of application

#### README.md file includes the following items:

* Splash Screen picture
* Unpopulated UI pictures
* Toast notification pictures
* Converted Currency pictures
* pictures of skill checks

#### Assignment Screenshots:

*Screenshot of Splash Screen:

![](SplashScreen.PNG)

*Screenshot of Blank UI:

![](BlankCurrency.PNG)

*Screenshot of Toast notification*:

![](ToastAndroidPic.PNG)

*Screenshot of Android Studio - Converted Currency:

![](ConvertedCurrency.PNG)





**Source Code Pictures**

![](converterpicMain.png)

**Source Code .txt**

[Converter App Main](ConverterApp.txt)