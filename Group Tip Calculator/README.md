# Group Tip Calculator App

## Thomas Palmieri

### Group Tip Second App:

1. Research drop-down menus in Android studio
2. Add background color/theme
3. Create launcher icon
4. Create Tip Calculator App
5. Screenshots of Application 

#### README.md file includes the following items:

* Screenshot of unpopulated interface
* Screenshot of populated interface
* Screenshot of Launcher icon
* Screenshot of of Source Code
* Links to Source Code

#### Assignment Screenshots:

*Screenshot of Unpopulated interface:

![](TipCalc1.png)

Screenshot of populated interface:

![](TipCalc2.png)



**Source Code Picture**

![](GroupPay1.png)

**Source Code .txt**

[Group Tip Main](GroupPayMain.txt)