public class MainActivity extends AppCompatActivity {
        double myBill=0.0;
        double totalCost=0.0;
        int numGuests=0.0;
        double costPerGuest=0.0;
        String groupTip="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHOmeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        final EditText bill=(EditText)findViewById(r.id.txtBill);
        final Spinner group = (Spinner)findViewById(R.id.spnGuests);
        Button cost =(Button)findViewById(R.id.btnCost);
        cost.setOnClickListener(new View.OnClickListener(){
            final TextView result= ((TextView)findViewById(R.id.txtResult t));
            @Override
            public void onClick(View v){
                groupTip=tip.getSelectedItem().toString();
                myBill=Double.parseDouble(bill.getText().toString());
                if((groupTip.equals("10%"))){
                    totalCost=myBill*1.10;
                }
                else if (groupTip.equals("20%")){
                    totalCost=myBill*1.20;
                }
                else{
                        totalCost=myBill*1.30;
                    }
                numGuests=Integer.parseInt(group.getSelectedItem().toString());
                costPerGuest=totalCost/numGuests;
                NumberFormat nf= NumberFormat.getCurrencyInstance(Local.US);
                result.setText("cost for all "+numGuests+" guests: "+nf.format(costPerGuest));
                }
            }
        }

    }

}