# Development And Coding

## Thomas Palmieri

### Readme Contents:

1. [FirstApp README.md](First Android Studio App/README.md "My HelloWorld README.md file")
    - First Application Hello World with button press dynamic string
    - First time dealing with android Studio
2. [GroupTip README.md](Group Tip Calculator/README.md "My Group Tip README.md file")
    - Create tip application
    - Screenshots of Application
3. [Currency Converter README.md](Currency Converter/README.md "My Currency Converter README.md file")
    - Research Splash Screen
    - Research radio buttons
    - Create Currency Converter Application
    - Screenshots of Application
4. [RSSFeed News App README.md](RSSFeed News App/README.md "My RSSFeed News App README.md file")
    - Research Dealing with RSS feed
    - Deal with formatting proper output
    - dealing with news feed
    - displaying proper screen with data
5. [Bash,SQL, Shell README.md](Bash,SQL, Shell/README.md "My Bash,SQL, Shell README.md file")
    - Questions from previous classes dealing with scripting and sql
    - read in names from file create custom queries biased off names
    - dealing with proper output
    - creating and managing data within databases through Linux and MySQL
6. [C# Game README.md](Game/README.md "My C# Game README.md file")
    - Camera Movement script
    - Enemy Damage script
    - Weapon Pick Up and Drop script
    - Weapon Manager script
    - Gif of running code and source code