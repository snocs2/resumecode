# Bash scripting and SQL

## Thomas Palmieri

#### README.md file includes the following items:

* Bash scripts
* SQL queries 

**Bash and SQL** 

One question from my exams

![](Q07_bash.PNG)

![](q07_bash1.PNG)

![](q07_bash2.PNG)

![](q07_bash3.PNG)



**Another Example question**

![](q03_bash.PNG)

![](q03_bash1.PNG)



**Another Question**

![](BashQ3_hw9.PNG)

![](BashQ3_hw9Pic.PNG)



**Another Question**

![](BashQ5_hw8.PNG)

![](BashQ5_hw8pic.PNG)