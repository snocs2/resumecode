# Unity FPS C#

## Thomas Palmieri

#### README.md file includes the following items:

* Camera movement Script
* Enemy Damage Script
* Weapon Manager Scrip
* Pick Up and Drop weapon Script
* Source Code
* Gif of everything

**UNITY GAME**



**Camera Movement**

![](CameraMovement.PNG)

**Source Code**

[Camera Movement Main.txt](CameraMovement.txt)



**Enemy Display**

![](EnemyDisplay1.PNG)

![](EnemyDisplay.PNG)

**Source Code**

[Enemy Damage.txt](EnemyDamage.txt)



**Weapon Pick Up and Drop Script**

![](PickUp.PNG)

**Source Code**

[Pick Up And Drop.txt](PickUp_DropScript.txt)



**Weapon Manager**

![](WeaponManager.PNG)

**Source Code**

[Weapon manager.txt](WeaponManager.txt)



**Gif of running game, Movement, camera, Inventory, weapon manger, shooting, rigidbody bullets ect**

![](GameDemo.gif)