# RSS Feed News Application

## Thomas Palmieri

### RSS Feed News App Requirements:

1. Main screens and article link
2. Use RSS feed
3. Display proper news articles

#### README.md file includes the following items:

* App Default screen
* Click Page screen
* News Website link
* Source Code pictures
* Source Code .txt links

#### Assignment Screenshots:

![](NewsPage1.png)

![](NewsPage2.png)

![](NewsWebsite.PNG)

**Source Code Pics**

**FileIO**

![](FileIO.png)

**Source Code**

[FileIO](FileIO.txt)



**ItemActivity**

![](ItemActivity.png)

**Source Code**

[ItemActivity](ItemActivity.txt)



**RSSFeed**

![](RSSFeed.png)

**Source Code**

[RSSFeed](RSSFeed.txt)



**RSSFeed Handler**

![](RSSFeedHandler.PNG)

**Source Code**

[RSSFeedHandler](RSSFeedHandler.txt)